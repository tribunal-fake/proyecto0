const getDB = require('../../database/getDB');

const deleteEntry = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    // Obtenemos el id de la entrada a eliminar.
    const { idEntry } = req.params;

    // Borramos la tabla de vostos y la entrada.
    //await connection.query(`DELETE FROM  entry_votes WHERE id = ?`, [idEntry]);
    await connection.query(`DELETE FROM  entries WHERE id = ?`, [idEntry]);

    res.send({
      status: 'ok',
      message: 'La entrada ha sido eliminada',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = deleteEntry;
