const getDB = require('../../database/getDB');
const { savePhoto } = require('../../helpers');

const editEntry = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    // Obtenemos el id de la entrada que queremos editar.
    const { idEntry } = req.params;

    // Obtenemos las propiedades del body.
    let { title, content } = req.body;

    // Si no recibimos ninguna propiedad lanzamos un error.
    if (!title && !content) {
      const error = new Error('Faltan campos');
      error.httpStatus = 400;
      throw error;
    }

    let savedPhoto;

    if (req.files?.media) {
      savedPhoto = await savePhoto(req.files.media, 1);
    }

    // Actualizamos la entrada.
    if (savedPhoto) {
      await connection.query(
        `UPDATE entries SET title = ?, content = ?, media = ?, modifiedAt = ? WHERE id = ?`,
        [title, content, savedPhoto, new Date(), idEntry]
      );
    } else {
      await connection.query(
        `UPDATE entries SET title = ?, content = ?, modifiedAt = ? WHERE id = ?`,
        [title, content, new Date(), idEntry]
      );
    }

    res.send({
      status: 'ok',
      message: 'Entrada actualizada',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = editEntry;
