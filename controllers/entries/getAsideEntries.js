const getDB = require('../../database/getDB');

///////////////////////////////////////
/////obtenemos las noticias del array del aside
const getAsideEntries = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        const [asideEntries] = await connection.query(
            `SELECT id, title, visits, media
            FROM entries
            ORDER BY visits DESC
            `
        );

        res.send({
            status: 'ok',
            data: [...asideEntries.slice(0, 5)],
        });
    } catch (error) {
        next(error);
    } finally {
        if (connection) connection.release();
    }
};

module.exports = getAsideEntries;
