const getDB = require('../../database/getDB');

const getCategory = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        const { idCategory } = req.params;

        if (idCategory > 10 || idCategory < 1) {
            const error = new Error('La categoría no existe');
            error.httpStatus = 404;
            throw error;
        }

        const [entries] = await connection.query(
            `SELECT  categories.name AS category, entries.idCategory, entries.id, entries.title, entries.media ,entries.content, entries.createdAt, entries.idUser, users.username, SUM(entry_votes.vote) AS votes
                    FROM entries
                    LEFT JOIN entry_votes ON (entries.id = entry_votes.idEntry)
                    LEFT JOIN users ON (entries.idUser = users.id)
                    LEFT JOIN categories ON (entries.idCategory = categories.id)
                     WHERE entries.idCategory = ?
                     GROUP BY entries.id
                     ORDER BY entries.createdAt DESC`,
            [idCategory]
        );

        res.send({
            status: 'ok',
            data: {
                entries,
            },
        });
    } catch (error) {
        next(error);
    } finally {
        if (connection) connection.release();
    }
};

module.exports = getCategory;
