const getDB = require('../../database/getDB');

const getComments = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        const { idEntry } = req.params;

        const [comments] = await connection.query(
            `SELECT  comments.id,comments.idEntry, comments.content, comments.createdAt, comments.idUser, users.username 
                    FROM comments
                    LEFT JOIN users ON (users.id = comments.idUser)
                     WHERE comments.idEntry = ?
                     GROUP BY comments.id
                     ORDER BY comments.createdAt DESC`,
            [idEntry]
        );

        res.send({
            status: 'ok',
            data: {
                comments,
            },
        });
    } catch (error) {
        next(error);
    } finally {
        if (connection) connection.release();
    }
};

module.exports = getComments;
