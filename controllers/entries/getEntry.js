const getDB = require('../../database/getDB');

const getEntry = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        // Obtenemos el id de la entrada.
        const { idEntry } = req.params;

        await connection.query(
            `
                UPDATE entries
                SET visits = IFNULL(visits, 0) + 1
                WHERE id = ?
    
            `,
            [idEntry]
        );

        // Obtenemos la información de la entrada de la base de datos: id, title, content, createdAt, idUser
        // y la media de votos.
        const [entry] = await connection.query(
            `
                SELECT entries.id, entries.title, entries.content, entries.createdAt, entries.idUser, entries.media, entries.visits, entries.idCategory, categories.name AS category ,users.username, SUM(entry_votes.vote) AS votes
                FROM entries
                LEFT JOIN entry_votes ON (entries.id = entry_votes.idEntry)
                LEFT JOIN users ON (entries.idUser = users.id)
                LEFT JOIN categories ON (entries.idCategory = categories.id)
                WHERE entries.id = ?
            `,
            [idEntry]
        );

        res.send({
            status: 'ok',
            data: {
                entry,
            },
        });
    } catch (error) {
        next(error);
    } finally {
        if (connection) connection.release();
    }
};

module.exports = getEntry;
