const getDB = require('../../database/getDB');

const getListCategories = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        const [listCategories] = await connection.query(
            `SELECT id, name
        FROM categories`
        );

        res.send({
            status: 'ok',
            data: listCategories,
        });
    } catch (error) {
        next(error);
    } finally {
        if (connection) connection.release();
    }
};

module.exports = getListCategories;
