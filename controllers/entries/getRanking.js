const getDB = require('../../database/getDB');

const getRanking = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const [entries] = await connection.query(
      `SELECT entries.id, entries.title, entries.content, entries.createdAt, entries.idUser, entries.media, entries.idCategory, categories.name, users.username, SUM(entry_votes.vote) AS votes
            FROM entries
            LEFT JOIN entry_votes ON (entries.id = entry_votes.idEntry)
            LEFT JOIN users ON (entries.idUser = users.id)
            LEFT JOIN categories ON (entries.idCategory = categories.id)
            GROUP BY entries.id
            ORDER BY votes DESC
            LIMIT 10
            `
    );

    res.send({
      status: 'ok',
      data: entries,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getRanking;
