const listEntries = require('./listEntries');
const getEntry = require('./getEntry');
const newEntry = require('./newEntry');
const voteEntry = require('./voteEntry');
const editEntry = require('./editEntry');
const deleteEntry = require('./deleteEntry');
const getCategory = require('./getCategory');
const getComments = require('./getComments');
const newComment = require('./newComment');
const getRanking = require('./getRanking');
const getAsideEntries = require('./getAsideEntries');
const getListCategories = require('./getListCategories');
const listUserEntries = require('./listUserEntries');

module.exports = {
    listEntries,
    getEntry,
    newEntry,
    voteEntry,
    editEntry,
    deleteEntry,
    getCategory,
    getComments,
    newComment,
    getRanking,
    getAsideEntries,
    getListCategories,
    listUserEntries,
};
