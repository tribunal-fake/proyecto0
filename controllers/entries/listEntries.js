const getDB = require('../../database/getDB');

const listEntries = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    // Obtenemos los posibles query params.
    const { search, order, direction } = req.query;

    // Posibles valores para "order".
    const validOrderOptions = ['title', 'createdAt', 'votes'];

    // Posibles valores para "direction".
    const validDirectionOptions = ['DESC', 'ASC'];

    // Establecemos que el orden por defecto sea la columna createdAt en caso
    // de que no venga ningún orden válido definido.
    const orderBy = validOrderOptions.includes(order) ? order : 'createdAt';

    // Establecemos la dirección por defecto en caso de que no venga ninguna
    // dirección válida dada.
    const orderDirection = validDirectionOptions.includes(direction)
      ? direction
      : 'DESC';

    // Variable donde almacenaremos las entradas.
    let entries;

    // Si la variable "search" no está vacía filtramos todas las entradas en cuya propiedad "title"
    // o "content" exista el string contenido en "search".
    if (search) {
      [entries] = await connection.query(
        `
                    SELECT entries.id, entries.title, entries.content, entries.createdAt, entries.visits, entries.modifiedAt, entries.media, entries.idCategory, categories.name AS category, entries.idUser, users.username, IFNULL(SUM(entry_votes.vote),0) AS votes
                    FROM entries 
                    LEFT JOIN entry_votes ON (entries.id = entry_votes.idEntry)
                    LEFT JOIN users ON (entries.idUser = users.id)
                    LEFT JOIN categories ON (entries.idCategory = categories.id)
                    WHERE entries.title LIKE ? OR entries.content LIKE ? OR users.username LIKE ?
                    GROUP BY entries.id
                    ORDER BY ${orderBy} ${orderDirection}
                `,
        [`%${search}%`, `%${search}%`, `%${search}%`]
      );
    }

    // En caso de que "search" no venga dado...
    else {
      [entries] = await connection.query(
        `
                    SELECT entries.id, entries.title, entries.content, entries.createdAt, entries.visits, entries.modifiedAt, entries.media, entries.idCategory, categories.name AS category, entries.idUser, users.username, IFNULL(SUM(entry_votes.vote),0) AS votes
                    FROM entries
                    LEFT JOIN entry_votes ON (entries.id = entry_votes.idEntry)
                    LEFT JOIN users ON (entries.idUser = users.id)
                    LEFT JOIN categories ON (entries.idCategory = categories.id)
                    GROUP BY entries.id
                    ORDER BY ${orderBy} ${orderDirection}
                `
      );
    }

    res.send({
      status: 'ok',
      data: {
        entries,
      },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = listEntries;
