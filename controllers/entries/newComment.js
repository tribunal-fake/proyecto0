const getDB = require('../../database/getDB');

const newComment = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        const idReqUser = req.userAuth.id;

        const { content } = req.body;
        const { idEntry } = req.params;

        const [
            user,
        ] = await connection.query(`SELECT id FROM users WHERE id = ?`, [
            idReqUser,
        ]);

        if (user.length < 1) {
            const error = new Error('El usuario no existe');
            error.httpStatus = 404;
            throw error;
        }

        const [newComment] = await connection.query(
            `
                INSERT INTO comments (content, idUser, idEntry, createdAt)
                VALUES (?, ?, ?, ?)
            `,
            [content, idReqUser, idEntry, new Date()]
        );

        const idComment = newComment.insertId;

        res.send({
            status: 'ok',
            message: 'El comentario ha sido creado',
        });
    } catch (error) {
        next(error);
    } finally {
        if (connection) connection.release();
    }
};

module.exports = newComment;
