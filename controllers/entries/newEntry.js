const getDB = require('../../database/getDB');

const { savePhoto, validate } = require('../../helpers');

const newEntrySchema = require('../../schemas/newEntrySchema');

const newEntry = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        // Validamos los datos del body.
        await validate(newEntrySchema, req.body);

        // Obtenemos el id del usuario que está creando la entrada.
        const idReqUser = req.userAuth.id;

        // Obtenemos las propiedades del body.
        const { title, content, idCategory } = req.body;

        const { media } = req.files;

        // Comprobamos que el usuario que realiza la "request" existe.
        const [
            user,
        ] = await connection.query(`SELECT id FROM users WHERE id = ?`, [
            idReqUser,
        ]);

        // Si el usuario no existe lanzamos un error.
        if (user.length < 1) {
            const error = new Error('El usuario no existe');
            error.httpStatus = 404;
            throw error;
        }

        const savedPhoto = await savePhoto(media, 1);

        // Creamos la entrada y guardamos el valor que retorna "connection.query".
        const [newEntry] = await connection.query(
            `
                INSERT INTO entries (title, content, media, idCategory, idUser, createdAt)
                VALUES (?, ?, ?, ?, ?, ?)
            `,
            [title, content, savedPhoto, idCategory, idReqUser, new Date()]
        );

        // Obtenemos el id de la entrada que acabamos de crear.
        const idEntry = newEntry.insertId;

        res.send({
            status: 'ok',
            message: 'La entrada ha sido creada',
        });
    } catch (error) {
        next(error);
    } finally {
        if (connection) connection.release();
    }
};

module.exports = newEntry;
