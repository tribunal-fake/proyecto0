//const { boolean } = require('joi');
const getDB = require('../../database/getDB');

const voteEntry = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    // Obtenemos el id de la entrada que va a ser votada.
    const { idEntry } = req.params;

    // Obtenemos el id del usuario que realiza el voto.
    const idReqUser = req.userAuth.id;

    // Obtenemos la votación.
    const { vote } = req.body;

    // Si falta el voto lanzamos un error.
    if (!vote) {
      const error = new Error('Faltan campos');
      error.httpStatus = 400;
      throw error;
    }

    // Si el voto no es  1 O -1 lanzamos un error.
    if (vote !== 1 && vote !== -1) {
      const error = new Error('El voto debe ser 1 o -1');
      error.httpStatus = 400;
      throw error;
    }

    // Obtenemos el usuario que realiza la request.
    const [user] = await connection.query(`SELECT id FROM users WHERE id = ?`, [
      idReqUser,
    ]);

    // Si el usuario no existe lanzamos un error.
    if (user.length < 1) {
      const error = new Error('El usuario no existe');
      error.httpStatus = 404;
      throw error;
    }

    // Obtenemos la propiedad "idUser" de la entrada.
    const [entry] = await connection.query(
      `SELECT idUser FROM entries WHERE id = ?`,
      [idEntry]
    );

    // Si el usuario que realiza la votación es el dueño de la entrada lanzamos
    // un error.
    if (entry[0].idUser === idReqUser) {
      const error = new Error('No puedes votar tu propia entrada');
      error.httpStatus = 403;
      throw error;
    }

    // Obtenemos el voto que el usuario que realiza la request haya podido
    // realizar sobre esta entrada.
    const [alreadyVote] = await connection.query(
      `SELECT id FROM entry_votes WHERE idUser = ? AND idEntry = ?`,
      [idReqUser, idEntry]
    );

    // Si el usuario ya ha votado esta entrada lanzamos un error.
    if (alreadyVote.length > 0) {
      const error = new Error('Esta entrada ya ha sido votada');
      error.httpStatus = 403;
      throw error;
    }

    // Añadimos el voto.
    await connection.query(
      `INSERT INTO entry_votes (vote, idUser, idEntry, createdAt) VALUES (?, ?, ?, ?)`,
      [vote, idReqUser, idEntry, new Date()]
    );

    res.send({
      status: 'ok',
      message: 'La entrada ha sido votada',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = voteEntry;
