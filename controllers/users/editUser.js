const getDB = require('../../database/getDB');

const { generateRandomString, verifyEmail } = require('../../helpers');

const editUser = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    // Obtenemos el id del usuario que queremos modificar.
    const { idUser } = req.params;

    // Obtenemos el username y el email.
    const { newUsername, newEmail, newBiography } = req.body;

    // Si no llega ningún dato a través del body lanzamos un error.
    if (!newUsername && !newEmail && !newBiography) {
      const error = new Error('Faltan campos');
      error.httpStatus = 400;
      throw error;
    }

    // Obtenemos el email,el username y la bio del usuario actual.
    const [user] = await connection.query(
      `SELECT email, username, biography FROM users WHERE id = ?`,
      [idUser]
    );

    // Mensaje que enviaremos en el "res.send".
    let message = 'Usuario actualizado';

    /**
     * ###########
     * ## Email ##
     * ###########
     *
     * En caso de que haya email debemos comprobar si es distinto al existente.
     *
     */
    if (newEmail && newEmail !== user[0].email) {
      // Comprobamos que el nuevo email no exista en la base de datos.
      const [email] = await connection.query(
        `SELECT id FROM users WHERE email = ?`,
        [newEmail]
      );

      // Si el email ya existe lanzamos un error.
      if (email.length > 0) {
        const error = new Error(
          'Ya existe un usuario con ese email en la base de datos'
        );
        error.httpStatus = 409;
        throw error;
      }

      // Creamos un código de registro de un solo uso.
      const registrationCode = generateRandomString(40);

      // Actualizamos el usuario en la base de datos junto al código de
      // registro.
      await connection.query(
        `
                    UPDATE users SET email = ?, registrationCode = ?, active = false, modifiedAt = ? 
                    WHERE id = ? 
                `,
        [newEmail, registrationCode, new Date(), idUser]
      );

      // Enviamos un mensaje de verificación al nuevo email del usuario.
      await verifyEmail(newEmail, registrationCode);

      // Actualizamos el mensaje que enviaremos al "res.send".
      message += ', comprueba tu nuevo email para activarlo';
    }

    /**
     * ##########
     * ## Name ##
     * ##########
     *
     * En caso de que haya nombre comprobamos si es distinto al existente.
     *
     */
    if (newUsername && newUsername !== user[0].name) {
      await connection.query(
        `UPDATE users SET username = ?, modifiedAt = ? WHERE id = ?`,
        [newUsername, new Date(), idUser]
      );
    }

    if (newBiography) {
      await connection.query(
        `UPDATE users SET biography = ?, modifiedAt = ? WHERE id = ?`,
        [newBiography, new Date(), idUser]
      );
    }

    // Obtenemos los datos del usuario del que queremos obtener la
    // información.
    const [userInfo] = await connection.query(
      `SELECT id, username, email, avatar, biography FROM users WHERE id = ?`,
      [idUser]
    );

    res.send({
      status: 'ok',
      message,
      data: { userInfo },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = editUser;
