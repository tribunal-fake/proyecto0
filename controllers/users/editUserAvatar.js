const getDB = require('../../database/getDB');

const { savePhoto, deletePhoto } = require('../../helpers');

const editUserAvatar = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    // Obtenemos el id del usuario que queremos editar.
    const { idUser } = req.params;

    //obtenemos el avatar

    const { avatar } = req.files;

    // Si no llega ningún dato lanzamos un error. Dado que la propiedad "req.files" podría no existir tenemos
    // que comprobar previamente si existe antes de comprobar si existe "req.files.avatar".
    if (!(req.files && req.files.avatar)) {
      const error = new Error('Faltan campos');
      error.httpStatus = 400;
      throw error;
    }

    // Obtenemos el avatar del usuario actual.
    const [user] = await connection.query(
      `SELECT avatar FROM users WHERE id = ?`,
      [idUser]
    );

    // Comprobamos si el usuario que queremos editar ya tiene avatar.
    if (user[0].avatar) {
      // Eliminamos el avatar del disco.
      await deletePhoto(user[0].avatar);
    }

    // Guardamos el nuevo avatar en el disco y obtenemos el nombre del
    // archivo.
    const avatarName = await savePhoto(avatar, 0);

    // Actualizamos el usuario con el nuevo avatar.
    await connection.query(
      `UPDATE users SET avatar = ?, modifiedAt = ? WHERE id = ?`,
      [avatarName, new Date(), idUser]
    );

    // Obtenemos los datos actualizados del usuario
    const [userInfo] = await connection.query(
      `SELECT id, username, email, avatar, biography FROM users WHERE id = ?`,
      [idUser]
    );

    res.send({
      status: 'ok',
      message: 'Usuario actualizado',
      data: { userInfo },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = editUserAvatar;
