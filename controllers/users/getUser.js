const getDB = require('../../database/getDB');

const getUser = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        // Obtenemos el id del usuario del que queremos obtener la
        // información.
        const { idUser } = req.params;

        // Obtener el id del usuario que hace la request.
        const idReqUser = req.userAuth.id;

        // Obtenemos los datos del usuario del que queremos obtener la
        // información.
        const [
            user,
        ] = await connection.query(
            `SELECT id, username, email, avatar, role, createdAt, biography FROM users WHERE id = ?`,
            [idUser]
        );

        // Objeto con información básica del usuario.
        const userInfo = {
            username: user[0].username,
            avatar: user[0].avatar,
            biography: user[0].biography,
        };

        // Si el usuario que solicita los datos es el dueño de dicho usuario (o es un admin)
        // agregamos información extra.
        if (user[0].id === idReqUser || req.userAuth.role === 'admin') {
            userInfo.email = user[0].email;
            userInfo.rol = user[0].role;
            userInfo.createdAt = user[0].createdAt;
        }

        res.send({
            status: 'ok',
            data: {
                userInfo,
            },
        });
    } catch (error) {
        next(error);
    } finally {
        if (connection) connection.release();
    }
};

module.exports = getUser;
