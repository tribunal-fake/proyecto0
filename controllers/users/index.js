const newUser = require('./newUser');
const validateUser = require('./validateUser');
const loginUser = require('./loginUser');
const getUser = require('./getUser');
const editUser = require('./editUser');
const editUserAvatar = require('./editUserAvatar');
const editUserPass = require('./editUserPass');
const recoverUserPass = require('./recoverUserPass');
const resetUserPass = require('./resetUserPass');
const deleteUser = require('./deleteUser');

module.exports = {
    newUser,
    validateUser,
    loginUser,
    getUser,
    editUser,
    editUserAvatar,
    editUserPass,
    recoverUserPass,
    resetUserPass,
    deleteUser,
};
