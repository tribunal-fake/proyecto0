const getDB = require('../../database/getDB');
const bcrypt = require('bcrypt');
const saltRounds = 10;

const {
    generateRandomString,
    verifyEmail,
    validate,
} = require('../../helpers');

const newUserSchema = require('../../schemas/newUserSchema');

const newUser = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        // Obtenemos los campos necesarios del body.
        const { email, password, username } = req.body;

        // Comprobamos si el email existe en la base de datos.
        const [user] = await connection.query(
            `SELECT id FROM users WHERE email = ?`,
            [email]
        );

        // Si el email ya existe lanzamos un error.
        if (user.length > 0) {
            const error = new Error('Ya existe un usuario con ese email');
            error.httpStatus = 409;
            throw error;
        }
        // Comprobamos si el username existe en la base de datos.
        const [userN] = await connection.query(
            `SELECT id FROM users WHERE username = ?`,
            [username]
        );

        // Si ese username ya existe lanzamos un error.
        if (userN.length > 0) {
            const error = new Error('Ya existe un usuario con ese nombre');
            error.httpStatus = 409;
            throw error;
        }
        // Validamos los datos del body.
        await validate(newUserSchema, req.body);

        // Creamos un código de registro de un solo uso.
        const registrationCode = generateRandomString(40);

        // Encriptamos la contraseña.
        const hashedPassword = await bcrypt.hash(password, saltRounds);

        // Guardamos el usuario en la base de datos.
        await connection.query(
            `INSERT INTO users (email, password, registrationCode, createdAt, username) VALUES (?, ?, ?, ?, ?)`,
            [email, hashedPassword, registrationCode, new Date(), username]
        );

        // Enviamos un mensaje de verificación al email del usuario.
        await verifyEmail(email, registrationCode);

        res.send({
            status: 'ok',
            message: 'Usuario registrado, comprueba tu email para activarlo',
        });
    } catch (error) {
        next(error);
    } finally {
        if (connection) connection.release();
    }
};

module.exports = newUser;
