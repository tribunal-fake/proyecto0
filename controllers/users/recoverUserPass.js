const getDB = require('../../database/getDB');

const { generateRandomString, sendMail } = require('../../helpers');

const recoverUserPass = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        // Obtenemos un email.
        const { email } = req.body;

        // Si falta el email lanzamos un error.
        if (!email) {
            const error = new Error('Faltan campos');
            error.httpStatus = 400;
            throw error;
        }

        // Comprobamos si el email existe en la base de datos.
        const [
            user,
        ] = await connection.query(`SELECT id FROM users WHERE email = ?`, [
            email,
        ]);

        // Si existe un usuario con ese email le enviamos un código de recuperación.
        if (user.length > 0) {
            // Generamos un código de recuperación.
            const recoverCode = generateRandomString(20);

            // Creamos el body del mensaje.
            const emailBody = `
                Se solicitó un cambio de contraseña para el usuario registrado con este email en la app Tribunal del Fake.

                El código de recuperación es: ${recoverCode}

                Si no has sido tú ignora este email.
            `;

            // Enviamos el email.
            await sendMail({
                to: email,
                subject: 'Cambio de contraseña en Tribunal de Fake',
                body: emailBody,
            });

            // Agregamos el código de recuperación al usuario con dicho email.
            await connection.query(
                `UPDATE users SET recoverCode = ?, modifiedAt = ? WHERE email = ?`,
                [recoverCode, new Date(), email]
            );
        }

        res.send({
            status: 'ok',
            message:
                'Si el email existe se ha enviado un código de recuperación',
        });
    } catch (error) {
        next(error);
    } finally {
        if (connection) connection.release();
    }
};

module.exports = recoverUserPass;
