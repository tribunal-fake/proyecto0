const getDB = require('./getDB');
const faker = require('faker/locale/es');
const { format } = require('date-fns');
const bcrypt = require('bcrypt');
//const { generateKey } = require('crypto');
const saltRounds = 10;

function formatDate(date) {
  return format(date, 'yyyyy-MM-dd HH:mm:ss');
}

function getRandomValue(min, max) {
  return Math.round(Math.random() * (max - min) + min);
}

async function main() {
  // Declaramos la variable fuera del bloque "try" para poder acceder
  // a ella en el bloque "finally".
  let connection;

  try {
    // Intentamos obtener una conexión libre con la base de datos.
    connection = await getDB();

    // Eliminamos las tablas existentes.
    await connection.query('SET FOREIGN_KEY_CHECKS = 0');
    await connection.query('DROP TABLE IF EXISTS entry_votes');
    await connection.query('DROP TABLE IF EXISTS entries');
    await connection.query('DROP TABLE IF EXISTS users');
    await connection.query('DROP TABLE IF EXISTS comments');
    await connection.query('DROP TABLE IF EXISTS categories');
    await connection.query('SET FOREIGN_KEY_CHECKS = 1');

    console.log('Tablas eliminadas');

    // Crear tabla de usuarios.
    await connection.query(`
            CREATE TABLE users (
                id INT PRIMARY KEY AUTO_INCREMENT,
                email VARCHAR(100) UNIQUE NOT NULL,
                password VARCHAR(512) NOT NULL,
                username VARCHAR(50) UNIQUE,
                avatar VARCHAR(50),
                active BOOLEAN DEFAULT false,
                deleted BOOLEAN DEFAULT false,
                role ENUM("admin", "normal") DEFAULT "normal" NOT NULL,
                biography VARCHAR(255) DEFAULT "Biografía",
                registrationCode VARCHAR(100),
                recoverCode VARCHAR(100),
                createdAt DATETIME NOT NULL,
                modifiedAt DATETIME
            )
        `);
    //crear la tabla de categorias
    await connection.query(`
            CREATE TABLE categories (
                id INT PRIMARY KEY AUTO_INCREMENT,
                name VARCHAR(25)
                
            )
    `); //"redes sociales", "actualidad", "politica", "musica", "curiosidades", "cine y television" ,"ciencia y tecnologia" ,"deportes" ,"gaming" , "otros") DEFAULT "otros" NOT NULL,

    // Crear la tabla de entradas.
    await connection.query(`
            CREATE TABLE entries (
                id INT PRIMARY KEY AUTO_INCREMENT,
                title VARCHAR(120) NOT NULL, 
                media VARCHAR(100) NOT NULL,
                content TEXT,                
                idUser INT NOT NULL,
                FOREIGN KEY (idUser) REFERENCES users(id) ON DELETE CASCADE,              
                idCategory INT NOT NULL,
                FOREIGN KEY (idCategory) REFERENCES categories(id),
                createdAt DATETIME NOT NULL,
                modifiedAt DATETIME,
                visits INT DEFAULT 0
            )
        `);

    // Crear la tabla de comentarios.
    await connection.query(`
            CREATE TABLE comments (
                id INT PRIMARY KEY AUTO_INCREMENT,
                content VARCHAR(255) NOT NULL,
                idEntry INT NOT NULL,
                FOREIGN KEY (idEntry) REFERENCES entries(id) ON DELETE CASCADE,
                idUser INT NOT NULL,
                FOREIGN KEY (idUser) REFERENCES users(id),
                createdAt DATETIME NOT NULL
            )
        `);

    // Crear la tabla de votos.
    await connection.query(`
            CREATE TABLE entry_votes (
                id INT PRIMARY KEY AUTO_INCREMENT,
                vote TINYINT DEFAULT 0,
                idEntry INT NOT NULL,
                FOREIGN KEY (idEntry) REFERENCES entries(id) ON DELETE CASCADE,
                idUser INT NOT NULL,
                FOREIGN KEY (idUser) REFERENCES users(id),
                createdAt DATETIME NOT NULL
            )
        `);

    console.log('Tablas creadas');

    const ADMIN_PASS = await bcrypt.hash('123456', saltRounds);

    await connection.query(`
        INSERT INTO users (email, password, username, active, role, createdAt, biography)
        VALUES (
            "tribunalfake20@gmail.com",
            "${ADMIN_PASS}",
            "Admin",
            true,
            "admin",
            "${formatDate(new Date())}",
            "biografia"

        )
    `);

    const USERS = 10;

    for (let i = 0; i < USERS; i++) {
      const email = faker.internet.email();
      const password = await bcrypt.hash('123456', saltRounds);
      const avatar = getRandomValue(1, 10);
      const biography = faker.lorem.sentence();
      const username = faker.name.findName();

      await connection.query(`
                INSERT INTO users (email, password, username, active, avatar, createdAt, biography )
                Values (
                    "${email}",
                    "${password}",
                    "${username}",
                    true,
                    "avatar${avatar}.png",
                    "${formatDate(new Date())}",
                    "${biography}"
                )
            `);
    }

    console.log('usuarios creados');

    const categories = [
      'Redes Sociales',
      'Actualidad',
      'Política',
      'Música',
      'Curiosidades',
      'Cine y Televisión',
      'Ciéncia y Tecnología',
      'Deportes',
      'Gaming',
      'Otros',
    ];

    for (const category of categories) {
      await connection.query(`
            INSERT INTO categories (name) VALUES ("${category}")`);
    }

    const ENTRIES = 100;

    for (let i = 0; i < ENTRIES; i++) {
      const title = faker.lorem.sentence();
      const content = faker.lorem.paragraph();
      const idUser = getRandomValue(2, 11);
      const idCategory = getRandomValue(1, 10);
      const media = getRandomValue(1, 10);
      const visits = getRandomValue(0, 100);

      await connection.query(`
                    INSERT INTO entries (title, content, media, idUser, idCategory, visits, createdAt)
                    VALUES(
                        "${title}",
                        "${content}",
                        "foto${media}.jpg",
                        "${idUser}",
                        "${idCategory}",
                        "${visits}",
                        "${formatDate(new Date())}"
                        
                    )
            `);
    }

    console.log('entradas creadas');

    const COMMENTS = 500;

    for (let i = 0; i < COMMENTS; i++) {
      const content = faker.lorem.sentence();
      const idUser = getRandomValue(2, 10);
      const idEntry = getRandomValue(1, 100);

      await connection.query(`
                    INSERT INTO comments ( content, idUser, idEntry, createdAt)
                    VALUES(
                        "${content}",
                        "${idUser}",
                        "${idEntry}",
                        "${formatDate(new Date())}"
                    )
            `);
    }

    console.log('comentarios creados');

    const VOTES = 250;

    for (let i = 0; i < VOTES; i++) {
      const vote = 1;
      const idUser = getRandomValue(2, 10);
      const idEntry = getRandomValue(1, 100);

      await connection.query(`
                    INSERT INTO entry_votes ( vote, idUser, idEntry, createdAt)
                    VALUES(                        
                        "${vote}",
                        "${idUser}",
                        "${idEntry}",
                        "${formatDate(new Date())}"
                    )
            `);
    }
    for (let i = 0; i < VOTES; i++) {
      const vote = -1;
      const idUser = getRandomValue(2, 10);
      const idEntry = getRandomValue(1, 100);

      await connection.query(`
                    INSERT INTO entry_votes ( vote, idUser, idEntry, createdAt)
                    VALUES(                        
                        "${vote}",
                        "${idUser}",
                        "${idEntry}",
                        "${formatDate(new Date())}"
                    )
            `);
    }

    console.log('votos creados');
  } catch (err) {
    console.error(err);
  } finally {
    // Si existe una conexión con la base de datos la liberamos.
    if (connection) connection.release();

    // Cerramos el proceso actual.
    process.exit(0);
  }
}

main();
