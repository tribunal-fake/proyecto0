const getDB = require('../database/getDB');

const canEditEntry = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    // Obtenemos el id de la entrada queremos editar.
    const { idEntry } = req.params;

    // Obtenemos el id del usuario que realiza la request.
    const idReqUser = req.userAuth.id;

    // Obtenemos el id del usuario de la entrada.
    const [owner] = await connection.query(
      `SELECT idUser FROM entries WHERE id = ?`,
      [idEntry]
    );

    //comprobamos si el usuario es administrador
    if (idReqUser !== 1) {
      // Si el usuario que realiza la request no es administrador o el propietario lanzamos
      // un error.
      if (owner[0].idUser !== idReqUser) {
        const error = new Error('No tienes permisos para editar esta entrada');
        error.httpStatus = 403;
        throw error;
      }
    }

    // Pasamos el control al siguiente middleware.
    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = canEditEntry;
