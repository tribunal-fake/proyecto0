const getDB = require('../database/getDB');

const canEditUser = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    // Obtenemos el id del usuario que queremos editar.
    const { idUser } = req.params;

    // Obtenemos el id del usuario que hace la request.
    const idReqUser = req.userAuth.id;

    //comprobamos si el usuario es administrador
    if (idReqUser !== 1) {
      // Lanzamos un error en caso de que no seamos dueños de ese usuario.
      if (Number(idUser) !== idReqUser) {
        const error = new Error('No tienes permisos para editar este usuario');
        error.httpStatus = 403;
        throw error;
      }
    }

    // Pasamos el control al siguiente middleware.
    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = canEditUser;
