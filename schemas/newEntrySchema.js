const Joi = require('joi');

const newEntrySchema = Joi.object().keys({
    title: Joi.string()
        .required()
        .min(3)
        .max(50)
        .regex(/[A-Za-z0-9 ]/)
        .error((errors) => {
            if (
                errors[0].code === 'any.required' ||
                errors[0].code === 'string.empty'
            ) {
                return new Error('La propiedad [title] es requerida');
            }

            return new Error(
                'La propiedad [title] debe tener entre 3 y 50 caracteres. Solo puede contener letras o números.'
            );
        }),

    content: Joi.string()
        .required()
        .min(20)
        .max(500)
        .error((errors) => {
            switch (errors[0].code) {
                case 'any.required':
                case 'string.empty':
                    return new Error('La propiedad [content] es requerida');

                default:
                    return new Error(
                        'La propiedad [content] debe tener entre 20 y 500 caracteres.'
                    );
            }
        }),
    media: Joi.any(),

    idCategory: Joi.string()
        .required()
        .min(1)
        .max(2)
        .error((errors) => {
            switch (errors[0].code) {
                case 'any.required':
                case 'string.empty':
                    return new Error('La propiedad [categoría] es requerida');

                default:
                    return new Error(
                        'La propiedad [categoría] debe ser del 1 al 10.'
                    );
            }
        }),
});

module.exports = newEntrySchema;
