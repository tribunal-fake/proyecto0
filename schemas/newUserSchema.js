const Joi = require('joi');

const newUserSchema = Joi.object().keys({
  email: Joi.string()
    .required()
    .email()
    .error((errors) => {
      if (
        errors[0].code === 'any.required' ||
        errors[0].code === 'string.empty'
      ) {
        return new Error('Se requiere un email');
      }

      return new Error('El email no es válido');
    }),

  password: Joi.string()
    .required()
    .min(6)
    .max(20)
    .error((errors) => {
      switch (errors[0].code) {
        case 'any.required':
        case 'string.empty':
          return new Error('Se requiere una contraseña');

        default:
          return new Error('La contraseña debe tener entre 6 y 20 caracteres');
      }
    }),
  username: Joi.string()
    .required()
    .min(4)
    .max(20)
    .regex(/[A-Za-z0-9 ]/)
    .error((errors) => {
      switch (errors[0].code) {
        case 'any.required':
        case 'string.empty':
          return new Error('Se requiere un nombre de usuario');

        default:
          return new Error(
            'El nombre de usuario debe tener entre 4 y 20 caracteres alfanuméricos'
          );
      }
    }),
});

module.exports = newUserSchema;
