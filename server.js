require('dotenv').config();
const morgan = require('morgan');
const express = require('express');
const fileUpload = require('express-fileupload');
const cors = require('cors');

const app = express();
app.use(cors());

const { PORT } = process.env;

/**
 * ###############################
 * ## Controladores de usuarios ##
 * ###############################
 */
const {
    newUser,
    validateUser,
    getUser,
    loginUser,
    editUser,
    editUserAvatar,
    editUserPass,
    recoverUserPass,
    resetUserPass,
    deleteUser,
} = require('./controllers/users');

//controladores entradas
const {
    listEntries,
    listUserEntries,
    getEntry,
    newEntry,
    voteEntry,
    editEntry,
    deleteEntry,
    getCategory,
    getComments,
    newComment,
    getRanking,
    getAsideEntries,
    getListCategories,
} = require('./controllers/entries');

////middlewares
const isAuth = require('./middlewares/isAuth');
const userExists = require('./middlewares/userExists');
const canEditUser = require('./middlewares/canEditUser');
const entryExists = require('./middlewares/entryExists');
const canEditEntry = require('./middlewares/canEditEntry');

//Ficheros estáticos
app.use('/fotos', express.static('./static/uploads'));

// Middleware que nos da información acerca de las peticiones que entren
// en el servidor.
app.use(morgan('dev'));

// Middleware que deserializa un body en formato "raw".
app.use(express.json());
// Middleware que deserializa un body en formato "form-data".
app.use(fileUpload());
// crear un nuevo usuario
app.post('/users', newUser);
// verificar usuario
app.get('/users/validate/:registrationCode', validateUser);
// logear un usuario
app.post('/users/login', loginUser);
// devuelve un usuario concreto
app.get('/users/:idUser', isAuth, userExists, getUser);
// para editar un usuario
app.put('/users/:idUser', isAuth, userExists, canEditUser, editUser);
// Editar el avatar de un usuario.
app.put(
    '/users/:idUser/avatar',
    isAuth,
    userExists,
    canEditUser,
    editUserAvatar
);
// Editar contraseña de usuario.
app.put(
    '/users/:idUser/password',
    isAuth,
    userExists,
    canEditUser,
    editUserPass
);

// Enviar un correo con un código de reseteo de contraseña a un usuario.
app.put('/users/password/recover', recoverUserPass);

// Cambiar la contraseña de un usuario con un código de registro.
app.put('/users/password/reset', resetUserPass);

// Anonimizar un usuario.
app.delete('/users/:idUser', isAuth, userExists, canEditUser, deleteUser);

//////// entradas

// listar entradas
app.get('/entries', listEntries);

//listar entradas de un usuario concreto
app.get('/entries/:idUser', listUserEntries, entryExists, userExists, isAuth);

// listar entrada concreta
app.get('/entry/:idEntry', entryExists, getEntry);
//nueva entrada
app.post('/entries', isAuth, newEntry);
// nueva votacion
app.post('/entries/votes/:idEntry', isAuth, entryExists, voteEntry);
// Editar una entrada.
app.put('/entries/:idEntry', isAuth, entryExists, canEditEntry, editEntry);
// Eliminar una entrada.
app.delete('/entries/:idEntry', isAuth, entryExists, canEditEntry, deleteEntry);
//Listar categorías
app.get('/categories', getListCategories);
//listar resultados de una categoria
app.get('/categories/:idCategory', getCategory);
//Listar comentarios
app.get('/entries/comments/:idEntry', entryExists, getComments);
//nuevo comentario
app.post('/entries/comments/:idEntry', entryExists, isAuth, newComment);
//ranquing
app.get('/ranking', getRanking);
//listar entradas del aside
app.get('/asideEntries', getAsideEntries);

/**
 * ######################
 * ## Middleware Error ##
 * ######################
 */
app.use((error, req, res, next) => {
    console.error(error);
    res.status(error.httpStatus || 500).send({
        status: 'error',
        message: error.message,
    });
});

/**
 * ##########################
 * ## Middleware Not Found ##
 * ##########################
 */
app.use((req, res) => {
    res.status(404).send({
        status: 'error',
        message: 'Not found',
    });
});

app.listen(PORT, () => {
    console.log(`Server listening at http://localhost:${PORT}`);
});
